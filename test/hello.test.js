'use strict'

const { expect } = require('chai');

const getHelloWorld = require('../hello');

describe('Hello World message generator', function() {
    it('should return the correct message', () => {
        const msg = getHelloWorld();
        expect(msg).to.be.a('string').that.is.equal("Hello World!");
    });
});
